class Test {
  Test._private() {
    print("In demo");
  }
  factory Test() {
    print("In demo factory");
    return Test._private();
  }
}

void main() {
  Test obj = new Test();
}

/*

Explanation:

  In the above code there is 2 constructor, 1st one is normal constructor but they 
make it private by using '_' this keyword, and 2nd constructor is Factory constructor 
that returns the object of his same class.
  Factory constructor is the one of main constructor in dart. Factory constructor is 
used for return the same class object or return child class object.
  we cannot prit anything in the code because there is no output in code.

*/