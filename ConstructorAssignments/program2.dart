class Employee {
  int? empId;
  String? empName;
  
  Employee(){}
  Employee(int empId, String empName){}
}
void main() {
Employee obj = new Employee();
}

/*

Explanation: 
  In the above code there is write a constructor 2 times in class.
In dart language there are everything is an object, therefore we cant write 
constructor 2 times in the class.

output : Error: Can't use 'Employee' because it is declared more than once.

*/