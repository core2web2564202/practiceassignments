class Company {
  int? empCount;
  String compName;
  Company({this.empCount, this.compName = "Deloitte"});
  void compInfo() {
    print(empCount);
    print(compName);
  }
}

void main() {
  Company obj1 = new Company(empCount: 100, compName: "Veritas");
  Company obj2 = new Company(compName: "Pubmatic", empCount: 200);
  obj1.compInfo();
  obj2.compInfo();
}

/*

Explaination:

  In the above code there is 1 Company class it has 2 instance veriables 1 constructor
and 1 comInfo method. 
  In class constructor there is Named Parameters to passing arrguments in instance 
veriables, In constructor Parameteres one of parameter is default Parameter 
e.i comName = "Deloitte".

  In the void main create 2 Objects of class Comapany & call comInfo method by 
2 different objects.
  In the 1st object passing arrguments in Sequencial format but in 2nd one passing
aggruments in unsequencial format but aslo its accepted because in class constructor 
Paramtere are Named Parameters thats why nothing error in code and output is:

Output: 
      100
      Veritas 
      200     
      Pubmatic

*/