class Player {
  int? jerNo;
  String? pName;

  const Player(this.jerNo, this.pName);
}
void main() {
  Player obj=const(45,”Rohit”);
}

/*

Explanation: 

  In the above code class Player has const constructor. But the veriables are not final.
rule no.1 if you want to make const constructor then make veriables final, thats why
error in code.

output: 
        
program8.dart:5:9: Error: Constructor is marked 'const' so all fields must be final.
  const Player(this.jerNo, this.pName);
        ^
program8.dart:2:8: Context: Field isn't final, but constructor is 'const'.
  int? jerNo;
       ^
program8.dart:3:11: Context: Field isn't final, but constructor is 'const'.
  String? pName;
          ^

*/