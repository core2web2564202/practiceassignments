class Demo {
  final int? x;
  final String? str;
  const Demo(this.x, this.str);
}

void main() {
  Demo obj1 = const Demo(10, "Core2web");
  print(obj1.hashCode);
  Demo obj2 = const Demo(10, "Biencaps");
  print(obj2.hashCode);
}

/*

Explanation: 
  In the above code we write 1 class, 2 final instance veriables and 1 constant 
constructor. In void main made 2 different object of class and pass different
arrguments and print the object's hascode.

output : 596048344
         610955364

  Objects hascode are different because we pass different arrguments in diffrent
objects.
  If we pass same arrguments in diffrent objects there will be print same 
hashcode, because const constructor made his only one object if data is same.

*/