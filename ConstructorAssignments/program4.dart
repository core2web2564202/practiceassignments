class Company {
  int empCount;
  String compName;
  Company(this.empCount, [this.compName = "Biencaps"]);
  void compInfo() {
    print(empCount);
    print(compName);
  }
}

void main() {
  Company obj1 = new Company(100);
  Company obj2 = new Company(200, "Pubmatic");
  obj1.compInfo();
  obj2.compInfo(); 
}

/*

Explanation:
  In the above code there is 1 Company class, 2 instance veriables, constructor 
and comInfo method.
  In void main there will be 2 diffrent Objects of class and call classInfo
method by diffrent objects.
  In class Company constructor there is one default Parameter value is comName = "Biancaps"
  In void main Object no.1, there is passing only one arrgument is empCount,
and 2nd arrgument is already given in constructor as default parameter value. Therefore,
output of Object no.1 is 100 & Biencaps
  If we want's to change the comName, we pass 2 arrgumets like Object no.2, and the 
output of Object no.2 is 200 & Pubmatic.

output: 100
        Biencaps
        200     
        Pubmatic
*/