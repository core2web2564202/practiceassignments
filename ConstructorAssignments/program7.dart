class Point {
  int x;
  int y;
}

void main() {
  Point obj = Point();
}

/*

Explanation: 

  In the above code there is 1 class & 2 Instance veriables but they not initialized
and also not constructor in class to initializes to veriables, thats why error in 
the code

output: 
program7.dart:2:7: Error: Field 'x' should be initialized because its type 'int' doesn't allow null.
  int x;
      ^
program7.dart:3:7: Error: Field 'y' should be initialized because its type 'int' doesn't allow null.
  int y;
      ^
*/