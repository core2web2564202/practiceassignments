class Test{
  final int x;
  final int y;
  
  const Test(this.x,this.y){
    print("In const constructor");
  }
}
void main(){
  Test obj= Test(10,20);
  print(obj.x);
}

/*
Explanation:
  In the above code there is constant (const) constructor and they give body to 
constant constructor but in dart language there is the rule for constant contructor
we cant write a body to constant constructor.

output: Error: A const constructor can't have a body.
        Try removing either the 'const' keyword or the body.
        const Test(this.x,this.y){
        ^^^^^
*/