int a = 10;

class Test {
  int x = 20;
  int y = 20;
  static num z = 30;
  Test(this.x, this.y, this.a);
  void fun() {
    print(a);
    print(x);
    print(y);
  }
}

void main() {
  Test obj = Test(10, 30, 40);
  obj.fun();
}

/*

Explanation: 

  In the above code "int a = 10;" is a global veriable and they assigned change 
value by class constructor. But globle veriable is only for use nothing for change
thats why error in the code.

output:
Error: 'a' isn't an instance field of this class.
  Test(this.x, this.y, this.a);
                            ^
*/