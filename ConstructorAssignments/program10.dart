class Demo {
  Demo() {
    print("In demo");
  }
  factory Demo() {
    print("In demo factory");
    return Demo();
  }
}

void main() {
  Demo obj = new Demo();
}

/*

Explanation:

  In the above code there is one noraml constructor and 2nd one is factory constructor.
But tha same name of constructors and we cant use same name to initialize 2 things
because in dart everything is an object, thats why error in the code.

output: 
program10.dart:5:11: Error: 'Demo' is already declared in this scope.
  factory Demo() {
          ^^^^
program10.dart:2:3: Context: Previous declaration of 'Demo'.
  Demo() {
  ^^^^
program10.dart:7:12: Error: Can't use 'Demo' because it is declared more than once. 
    return Demo();
           ^
program10.dart:12:18: Error: Can't use 'Demo' because it is declared more than once.
  Demo obj = new Demo();
                 ^

*/