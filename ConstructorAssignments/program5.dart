class Company {
  int? x;
  String? str;
  Company(this.x, {this.str = "Core2webe"});
  void compInfo() {
    print(x);
    print(str);
  }
}

void main() {
  Company obj1 = new Company(100);
  Company obj2 = new Company(200, "Incubator");
  obj1.compInfo();
  obj2.compInfo();
}

/*

Explanation: 
  In the above code there is normal constructor with Default Parameters in type
of Named Parameters{}.
  In constructor we write named paramters in '{}' brase then we will pass values 
in object like "name of the variable : value".
  In the 2nd object They pass arrguments like narmal Parameters but parameters are
Named type, thats why Error in the code

output : Error: Too many positional arguments: 1 allowed, but 2 found.
         Try removing the extra positional arguments.
         Company obj2 = new Company(200, "Incubator");
                                   ^
*/